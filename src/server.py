from mpd import MPDClient

from gi.repository import GObject, GLib, Adw


class Server(GObject.GObject):

    name = GObject.Property(type=str, default='MPD Server')
    host = GObject.Property(type=str, default='host')
    port = GObject.Property(type=int)

    """
    __gsignals__ = {
        'connection-lost' : (GObject.SIGNAL_RUN_FIRST, None, ()),
        'playlist-changed' : (GObject.SIGNAL_RUN_FIRST, None, ()),
    }
    """


    def dispose(self):
        if self.idle_source:
            GLib.source_remove(self.idle_source)
            self.idle_source = 0

    def __init__(self, name="", host="localhost", port=0):
        GObject.GObject.__init__(self)

        self.host = host
        self.port = port if port > 0 else 6600

        self.idle_source = 0

        self.client = MPDClient()

        self.connect()
        self.update_current_song()

        print(self.client.status())
        print()
        print(self.props.status)
        print(self.props.currentsong)

    @GObject.Property(type=bool, default=False)
    def connected(self):
        return self.is_connected

    @GObject.Property(type=str)
    def status(self):
        return self.client.status()

    @GObject.Property(type=str)
    def currentsong(self):
        return self.current_song['title']

    @GObject.Property(type=str)
    def version(self):
        return self.client.mpd_version #if self.props.connected else ""

    def update_current_song(self):
        if not self.connected:
            return

        self.current_song = self.client.currentsong()

    def do_idle(self):
        systems = self.client.idle()
        if 'player' in systems:
            self.update_current_song()


    """
    @GObject.Property(type=bool, default=False)
    def active(self):
        'Read-write'
        return self.active

    @active.setter
    def active(self, enable):
        if self.active == enable:
            return

        self.active

        if enable:
            ok = self.connect()
        else:
            ok = self.disconnect()

        if ok:
            self.active = enable

"""

    def is_reachable(self):
        return True

    def update_status(self):
        new_status = self.client.status()
        print(new_status)


    def connect(self):
        try:
            # set a timeout here!
            # this needs to run in a different thread
            self.client.connect(self.host, self.port)
        except:
            return False
        """
        except mpd.base.ConnectionError:
            return False
        except ConnectionRefusedError:
            return False
        except socket.gaierror (getaddrinfo)


        """

        self.is_connected = True
        self.timeout = 0
        return True

    def disconnect(self):
        self.is_connected = False

        return True

# g_task_run_in_thread (mit AsyncReadyCallback) fuer self.client.idle
# oder connecten oder so!
